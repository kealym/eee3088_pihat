# Manufacturing Steps

## Step one
Using the bill of materials [here](https://gitlab.com/kealym/eee3088_pihat/-/blob/master/uHat_BOM.xml) all components need to be gathered. 

## Step two
Place and solder all resistors, capacitors, inductors and diodes as these are the lowest components. 

## Step three
Place and solder all ICs (LT8741, LM1875 and LM3194) making sure that no pads are shorted,

## Step four
Place and solder all jacks, connectors and the 10-segment LED bar. These components have tight tolerances, but will fit. 
