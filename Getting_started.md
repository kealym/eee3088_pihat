# Getting started

## Step one - setup
Connect micro hat to the pi zero. Plug the 20v barrel power connector into the barrel jack on the left of the board. 

## Step two - audio connection
Turn the volume and frequency knob as far left as they can go. Plug an aux cable signal source into the 3.5mm audio connector on the board. 

## Step three - audio testing
Turn the volume knob mid way and select a frequency, adjust the volume knob to a level where LEDs are lit up mid way on the LED bar.
Using this as your level you can sweep through frequencies viewing the respective audio levels.  
