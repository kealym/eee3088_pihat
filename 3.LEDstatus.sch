EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7650 6950 0    157  ~ 31
VU Meter - PiHat\n
$Comp
L Device:R_Small_US R13
U 1 1 60BB6191
P 4650 4800
F 0 "R13" H 4718 4846 50  0000 L CNN
F 1 "10k" H 4718 4755 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4650 4800 50  0001 C CNN
F 3 "~" H 4650 4800 50  0001 C CNN
	1    4650 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 4300 4650 4700
$Comp
L power:GND #PWR02
U 1 1 60BD70B1
P 2300 6850
F 0 "#PWR02" H 2300 6600 50  0001 C CNN
F 1 "GND" H 2305 6677 50  0000 C CNN
F 2 "" H 2300 6850 50  0001 C CNN
F 3 "" H 2300 6850 50  0001 C CNN
	1    2300 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 6850 2300 6550
$Comp
L Device:R_POT VolumeThreshholdPot1
U 1 1 60BE8A90
P 1600 6200
F 0 "VolumeThreshholdPot1" H 1531 6246 50  0000 R CNN
F 1 "47k" H 1531 6155 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Vishay_248GJ-249GJ_Single_Vertical" H 1600 6200 50  0001 C CNN
F 3 "~" H 1600 6200 50  0001 C CNN
	1    1600 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 5650 1600 5650
Wire Wire Line
	1600 5650 1600 6050
Wire Wire Line
	1750 6200 2000 6200
Wire Wire Line
	2000 6200 2000 6550
Wire Wire Line
	2000 6550 2300 6550
Connection ~ 2300 6550
$Comp
L pspice:CAP C9
U 1 1 60BEFD45
P 2900 4600
F 0 "C9" H 3078 4646 50  0000 L CNN
F 1 "10n" H 3078 4555 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L10.0mm_D6.0mm_P15.00mm_Horizontal" H 2900 4600 50  0001 C CNN
F 3 "~" H 2900 4600 50  0001 C CNN
	1    2900 4600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 60BF2130
P 2650 5000
F 0 "#PWR03" H 2650 4750 50  0001 C CNN
F 1 "GND" H 2655 4827 50  0000 C CNN
F 2 "" H 2650 5000 50  0001 C CNN
F 3 "" H 2650 5000 50  0001 C CNN
	1    2650 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 4600 2650 5000
Wire Wire Line
	3150 4600 3400 4600
Wire Wire Line
	2100 4250 2450 4250
Text GLabel 1100 5300 0    50   Input ~ 10
FSigIn
Wire Wire Line
	1600 6350 1600 6550
Wire Wire Line
	1600 6550 2000 6550
Connection ~ 2000 6550
Text GLabel 4650 4300 1    50   Output ~ 0
+5V
Text GLabel 2100 4250 0    50   Output ~ 0
+5V
Text Notes 7450 7500 0    50   ~ 0
RSXERI002, BRMKEA001, DVLKAR008
Text Notes 7400 7250 0    50   ~ 0
1
Text Notes 7300 7250 0    50   ~ 0
1
Text Notes 10600 7650 0    50   ~ 0
1
Text Notes 8150 7650 0    50   ~ 0
04/06/2021
$Comp
L LED:HLCP-J100_2 BAR1
U 1 1 60EA1062
P 4150 5700
F 0 "BAR1" H 4150 6367 50  0000 C CNN
F 1 "HLCP-J100_2" H 4150 6276 50  0000 C CNN
F 2 "Display:HLCP-J100" H 4150 4900 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-1798EN" H 2150 5900 50  0001 C CNN
	1    4150 5700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4650 6100 4350 6100
Wire Wire Line
	4350 6000 4650 6000
Connection ~ 4650 6000
Wire Wire Line
	4650 6000 4650 6100
Wire Wire Line
	4350 5900 4650 5900
Connection ~ 4650 5900
Wire Wire Line
	4650 5900 4650 6000
Wire Wire Line
	4350 5800 4650 5800
Wire Wire Line
	4650 4900 4650 5200
Connection ~ 4650 5800
Wire Wire Line
	4650 5800 4650 5900
Wire Wire Line
	4350 5700 4650 5700
Connection ~ 4650 5700
Wire Wire Line
	4650 5700 4650 5800
Wire Wire Line
	4350 5600 4650 5600
Connection ~ 4650 5600
Wire Wire Line
	4650 5600 4650 5700
Wire Wire Line
	4350 5500 4650 5500
Connection ~ 4650 5500
Wire Wire Line
	4650 5500 4650 5600
Wire Wire Line
	4350 5400 4650 5400
Connection ~ 4650 5400
Wire Wire Line
	4650 5400 4650 5500
Wire Wire Line
	4350 5300 4650 5300
Connection ~ 4650 5300
Wire Wire Line
	4650 5300 4650 5400
Wire Wire Line
	4350 5200 4650 5200
Connection ~ 4650 5200
Wire Wire Line
	4650 5200 4650 5300
Wire Wire Line
	3400 4600 3400 4250
Wire Wire Line
	3400 4250 2450 4250
Connection ~ 2450 4250
$Comp
L Driver_LED:LM3914V U5
U 1 1 612585EE
P 3400 5600
F 0 "U5" H 3400 6381 50  0000 C CNN
F 1 "LM3914V" H 3400 6290 50  0000 C CNN
F 2 "Package_LCC:PLCC-20" H 3400 5600 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/lm3914.pdf" H 3400 5600 50  0001 C CNN
	1    3400 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R12
U 1 1 60BD81F3
P 2050 5500
F 0 "R12" V 2255 5500 50  0000 C CNN
F 1 "5k" V 2164 5500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 2050 5500 50  0001 C CNN
F 3 "~" H 2050 5500 50  0001 C CNN
	1    2050 5500
	0    -1   -1   0   
$EndComp
Connection ~ 1750 5650
Wire Wire Line
	3800 5200 3950 5200
Wire Wire Line
	3800 5300 3950 5300
Wire Wire Line
	3800 5400 3950 5400
Wire Wire Line
	3800 5500 3950 5500
Wire Wire Line
	3800 5600 3950 5600
Wire Wire Line
	3800 5700 3950 5700
Wire Wire Line
	3800 5800 3950 5800
Wire Wire Line
	3800 5900 3950 5900
Wire Wire Line
	3800 6000 3950 6000
Wire Wire Line
	3800 6100 3950 6100
Wire Wire Line
	3400 6300 3400 6550
Wire Wire Line
	3400 6550 2750 6550
Wire Wire Line
	3400 5000 3400 4600
Connection ~ 3400 4600
Wire Wire Line
	1750 5900 3000 5900
Wire Wire Line
	1750 5650 1750 5900
Wire Wire Line
	2450 6100 3000 6100
Wire Wire Line
	2450 4250 2450 6100
Wire Wire Line
	3000 5300 1100 5300
Wire Wire Line
	3000 5600 2750 5600
Wire Wire Line
	2750 5600 2750 6550
Connection ~ 2750 6550
Wire Wire Line
	2750 6550 2300 6550
Wire Wire Line
	1750 5500 1950 5500
Wire Wire Line
	1750 5500 1750 5650
Wire Wire Line
	2150 5500 2300 5500
Wire Wire Line
	2300 5500 2300 5800
Wire Wire Line
	2300 5800 3000 5800
Connection ~ 2300 5500
Wire Wire Line
	2300 5500 3000 5500
$EndSCHEMATC
