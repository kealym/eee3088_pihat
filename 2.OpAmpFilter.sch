EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7400 7500 0    50   ~ 0
BRMKEA001 - Subsystem 2 - Filter
$Comp
L Amplifier_Audio:LM1875 U2
U 1 1 60B752C6
P 7300 2450
F 0 "U2" H 7644 2496 50  0000 L CNN
F 1 "LM1875" H 7644 2405 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-5_P3.4x3.7mm_StaggerOdd_Lead3.8mm_Vertical" H 7300 2450 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm1875.pdf" H 7300 2450 50  0001 C CNN
	1    7300 2450
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Audio:LM1875 U1
U 1 1 60B77EDF
P 5350 3550
F 0 "U1" H 5694 3596 50  0000 L CNN
F 1 "LM1875" H 5694 3505 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-5_P3.4x3.7mm_StaggerOdd_Lead3.8mm_Vertical" H 5350 3550 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm1875.pdf" H 5350 3550 50  0001 C CNN
	1    5350 3550
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 60B79DB6
P 6250 2550
F 0 "RV1" H 6181 2596 50  0000 R CNN
F 1 "R_POT" H 6181 2505 50  0000 R CNN
F 2 "" H 6250 2550 50  0001 C CNN
F 3 "~" H 6250 2550 50  0001 C CNN
	1    6250 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 60B7E515
P 6250 3200
F 0 "C2" H 6365 3246 50  0000 L CNN
F 1 "20n" H 6365 3155 50  0000 L CNN
F 2 "" H 6288 3050 50  0001 C CNN
F 3 "~" H 6250 3200 50  0001 C CNN
	1    6250 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R Rser1
U 1 1 60B7F627
P 3350 2350
F 0 "Rser1" V 3143 2350 50  0000 C CNN
F 1 "100k" V 3234 2350 50  0000 C CNN
F 2 "" V 3280 2350 50  0001 C CNN
F 3 "~" H 3350 2350 50  0001 C CNN
	1    3350 2350
	0    1    1    0   
$EndComp
$Comp
L Device:R Rshu1
U 1 1 60B81EBB
P 4100 3250
F 0 "Rshu1" H 4170 3296 50  0000 L CNN
F 1 "100k" H 4170 3205 50  0000 L CNN
F 2 "" V 4030 3250 50  0001 C CNN
F 3 "~" H 4100 3250 50  0001 C CNN
	1    4100 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 60B83A4F
P 6250 3650
F 0 "R1" H 6320 3696 50  0000 L CNN
F 1 "1k" H 6320 3605 50  0000 L CNN
F 2 "" V 6180 3650 50  0001 C CNN
F 3 "~" H 6250 3650 50  0001 C CNN
	1    6250 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 60B83D3A
P 6250 4100
F 0 "R2" H 6320 4146 50  0000 L CNN
F 1 "10k" H 6320 4055 50  0000 L CNN
F 2 "" V 6180 4100 50  0001 C CNN
F 3 "~" H 6250 4100 50  0001 C CNN
	1    6250 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 60B83F51
P 6250 4550
F 0 "R3" H 6320 4596 50  0000 L CNN
F 1 "10k" H 6320 4505 50  0000 L CNN
F 2 "" V 6180 4550 50  0001 C CNN
F 3 "~" H 6250 4550 50  0001 C CNN
	1    6250 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 60B847E0
P 4400 3250
F 0 "C1" H 4515 3296 50  0000 L CNN
F 1 "20n" H 4515 3205 50  0000 L CNN
F 2 "" H 4438 3100 50  0001 C CNN
F 3 "~" H 4400 3250 50  0001 C CNN
	1    4400 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 2350 4100 2350
Wire Wire Line
	6250 2400 6250 2350
Connection ~ 6250 2350
Wire Wire Line
	6250 2350 7000 2350
Wire Wire Line
	3200 2350 2650 2350
Wire Wire Line
	4100 3100 4100 2350
Connection ~ 4100 2350
Wire Wire Line
	4100 3400 4100 3800
Wire Wire Line
	4400 3100 4400 2350
Wire Wire Line
	4100 2350 4400 2350
Connection ~ 4400 2350
Wire Wire Line
	4400 2350 6250 2350
$Comp
L power:GND #PWR0102
U 1 1 60B88BA9
P 4100 3800
F 0 "#PWR0102" H 4100 3550 50  0001 C CNN
F 1 "GND" H 4105 3627 50  0000 C CNN
F 2 "" H 4100 3800 50  0001 C CNN
F 3 "" H 4100 3800 50  0001 C CNN
	1    4100 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 60B8AD99
P 4400 3800
F 0 "#PWR0103" H 4400 3550 50  0001 C CNN
F 1 "GND" H 4405 3627 50  0000 C CNN
F 2 "" H 4400 3800 50  0001 C CNN
F 3 "" H 4400 3800 50  0001 C CNN
	1    4400 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3800 4400 3400
$Comp
L power:GND #PWR0104
U 1 1 60B8D0B0
P 6250 2700
F 0 "#PWR0104" H 6250 2450 50  0001 C CNN
F 1 "GND" H 6255 2527 50  0000 C CNN
F 2 "" H 6250 2700 50  0001 C CNN
F 3 "" H 6250 2700 50  0001 C CNN
	1    6250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 2550 6450 2550
Wire Wire Line
	6450 2550 6450 2950
Wire Wire Line
	6450 2950 6250 2950
Wire Wire Line
	6250 2950 6250 3050
Wire Wire Line
	6250 2950 4800 2950
Wire Wire Line
	4800 2950 4800 3550
Wire Wire Line
	4800 3550 5050 3550
Connection ~ 6250 2950
Wire Wire Line
	6250 3350 6250 3450
Wire Wire Line
	6250 3800 6250 3850
Wire Wire Line
	6250 4250 6250 4300
Wire Wire Line
	6250 4700 6250 5000
$Comp
L power:GND #PWR0105
U 1 1 60B91D21
P 6250 5000
F 0 "#PWR0105" H 6250 4750 50  0001 C CNN
F 1 "GND" H 6255 4827 50  0000 C CNN
F 2 "" H 6250 5000 50  0001 C CNN
F 3 "" H 6250 5000 50  0001 C CNN
	1    6250 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3450 6250 3450
Connection ~ 6250 3450
Wire Wire Line
	6250 3450 6250 3500
Wire Wire Line
	6250 3850 8100 3850
Wire Wire Line
	8100 3850 8100 2450
Wire Wire Line
	8100 2450 7600 2450
Connection ~ 6250 3850
Wire Wire Line
	6250 3850 6250 3950
Wire Wire Line
	6250 4300 5800 4300
Wire Wire Line
	5800 4300 5800 3650
Wire Wire Line
	5800 3650 5650 3650
Connection ~ 6250 4300
Wire Wire Line
	6250 4300 6250 4400
Wire Wire Line
	8100 2450 8500 2450
Connection ~ 8100 2450
$Comp
L power:+5V #PWR0106
U 1 1 60B9495B
P 7200 2150
F 0 "#PWR0106" H 7200 2000 50  0001 C CNN
F 1 "+5V" H 7215 2323 50  0000 C CNN
F 2 "" H 7200 2150 50  0001 C CNN
F 3 "" H 7200 2150 50  0001 C CNN
	1    7200 2150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 60B974FD
P 5450 3850
F 0 "#PWR0107" H 5450 3700 50  0001 C CNN
F 1 "+5V" H 5465 4023 50  0000 C CNN
F 2 "" H 5450 3850 50  0001 C CNN
F 3 "" H 5450 3850 50  0001 C CNN
	1    5450 3850
	-1   0    0    1   
$EndComp
$Comp
L power:-5V #PWR0108
U 1 1 60B97B30
P 5450 3250
F 0 "#PWR0108" H 5450 3350 50  0001 C CNN
F 1 "-5V" H 5465 3423 50  0000 C CNN
F 2 "" H 5450 3250 50  0001 C CNN
F 3 "" H 5450 3250 50  0001 C CNN
	1    5450 3250
	1    0    0    -1  
$EndComp
$Comp
L power:-5V #PWR0109
U 1 1 60B98FE7
P 7200 2750
F 0 "#PWR0109" H 7200 2850 50  0001 C CNN
F 1 "-5V" H 7215 2923 50  0000 C CNN
F 2 "" H 7200 2750 50  0001 C CNN
F 3 "" H 7200 2750 50  0001 C CNN
	1    7200 2750
	-1   0    0    1   
$EndComp
Wire Wire Line
	7000 2550 6850 2550
Wire Wire Line
	6850 2550 6850 3450
Wire Wire Line
	6850 3450 6250 3450
Text GLabel 2650 2350 0    50   Input ~ 0
Vin
Text GLabel 8500 2450 2    50   Input ~ 0
Vout
$EndSCHEMATC
